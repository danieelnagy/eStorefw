package com.yajava.ft.beans;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.yajava.bt.service.CartService;
import com.yajava.bt.service.OrderService;
import com.yajava.dt.model.Customer;
import com.yajava.dt.model.Order;
import com.yajava.dt.model.Product;

@Named("orderBean")
@ApplicationScoped
public class OrderBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Inject
	OrderService orderService;
	Order order;
	private List<Order> orders;


	@PostConstruct
	public void init() throws IOException {
		this.orders = orderService.getOrders();
	}

	public String createOrder(int customerId) throws IOException {

		orderService.createOrder(customerId);
		return "ja";
	}

	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}
	
	

	
	
}
