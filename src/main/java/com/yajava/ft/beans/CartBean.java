package com.yajava.ft.beans;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.yajava.bt.service.CartService;
import com.yajava.dt.model.Product;


@Named("cartBean")
@RequestScoped
public class CartBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Inject
	private CartService cartService;
	private List<Product> productsCart;
	private double totalSum = 0;
	private Product product = new Product();

	
	public List<Product> getProductsCart() {
		return productsCart;
	}


	public void setProductsCart(List<Product> productsCart) {
		this.productsCart = productsCart;
	}


	private double calculateCart() {
		double temp = 0;
		for (Product element : productsCart) {
			temp = temp + element.getPrice();
		}
		return temp;
	}


	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@PostConstruct
	public void init() throws IOException {
		this.productsCart = cartService.getCart();
		this.totalSum = calculateCart();

	}


	public double getTotalSum() {
		return totalSum;
	}


	public void setTotalSum(double totalSum) {
		this.totalSum = totalSum;
	}

	
}
