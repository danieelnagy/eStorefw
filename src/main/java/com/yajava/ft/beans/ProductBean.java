package com.yajava.ft.beans;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.yajava.bt.service.ProductService;
import com.yajava.dt.model.Product;

@Named("productBean")
@SessionScoped
public class ProductBean implements Serializable {
	

	private static final long serialVersionUID = 1L;
	@Inject
	private ProductService productService;
	private List<Product> products;
	private Product product = new Product();
	
	
	@PostConstruct
	
	public void init() throws IOException {

		this.products = productService.getProduts();

	}

	public void addToCart(Product p) throws IOException {

		productService.addToCart(p.getId());
		System.out.println(p.getId());

	}
	public ProductService getProductService() {
		return productService;
	}


	public void setProductService(ProductService productService) {
		this.productService = productService;
	}


	public List<Product> getProducts() {
		return products;
	}


	public void setProducts(List<Product> products) {
		this.products = products;
	}


	public Product getProduct() {
		return product;
	}


	public void setProduct(Product product) {
		this.product = product;
	}
	




}
