package com.yajava.bt.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.yajava.dt.model.Product;

@ApplicationScoped
public class ProductService implements Serializable{
	
	private static final long serialVersionUID = -4325770569991189411L;
	public static ServiceClient service = ServiceClient.getInstance();

	public List<Product> getProduts() throws IOException  {

		URL url = new URL(ServiceClient.getGetProducts());
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.setDoOutput(true);
		BufferedReader br = new BufferedReader(new InputStreamReader((connection.getInputStream())));
		StringBuilder sb = new StringBuilder();
		String output;
		while ((output = br.readLine()) != null) {
			sb.append(output);
		}
		br.close();
		ObjectMapper mapper = new ObjectMapper();

		List<Product> products = mapper.readValue(sb.toString(), new TypeReference<List<Product>>() {
		});

		return products;
	}
	
	public void addToCart(int id) throws IOException {
		URL url = new URL(ServiceClient.getAddToCart() + id);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json; utf-8");
		con.setRequestProperty("Accept", "application/json");
		con.setDoOutput(true);

		try (BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
			StringBuilder response = new StringBuilder();
			String responseLine = null;
			while ((responseLine = br.readLine()) != null) {
				response.append(responseLine.trim());
			}
			System.out.println(response.toString());
		}
	}

}
