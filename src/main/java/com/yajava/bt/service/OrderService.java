package com.yajava.bt.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yajava.dt.model.Order;


public class OrderService implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ServiceClient service = ServiceClient.getInstance();

	public void createOrder(int customerID) throws IOException {


		URL url = new URL(ServiceClient.getCreateOrder() + customerID);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type", "application/json; utf-8");
		connection.setRequestProperty("Accept", "application/json");
		connection.setDoOutput(true);

		try (BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"))) {
			StringBuilder response = new StringBuilder();
			String responseLine = null;
			while ((responseLine = br.readLine()) != null) {
				response.append(responseLine.trim());
			}
			System.out.println(response.toString());
		}

	}
	


	public List<Order> getOrders() throws IOException {

		URL url = new URL(ServiceClient.getGetOrders());
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.setDoOutput(true);
		BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
		StringBuilder sb = new StringBuilder();
		String output;
		while ((output = br.readLine()) != null) {
			sb.append(output);
		}
		br.close();
		ObjectMapper mapper = new ObjectMapper();

		List<Order> orders = mapper.readValue(sb.toString(), new TypeReference<List<Order>>() {
		});
		return orders;
	}
	

}
