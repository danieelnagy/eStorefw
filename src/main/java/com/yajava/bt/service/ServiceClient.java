package com.yajava.bt.service;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ServiceClient {

	/* POST USE ID */
	private static final String GET_CUSTOMERS = "http://localhost:8080/eStore/estore/customers/getCustomers";
	/* POST USE ID */
	private static final String ADD_TO_CART = "http://localhost:8080/eStore/estore/orders/addToCart/";
	private static final String CREATE_ORDER = "http://localhost:8080/eStore/estore/orders/createOrder/";
	private static final String GET_CART = "http://localhost:8080/eStore/estore/orders/getCart";
	private static final String GET_ORDERS = "http://localhost:8080/eStore/estore/orders/getOrders";
	private static final String GET_PRODUCTS = "http://localhost:8080/eStore/estore/products/getProducts";

	private static ServiceClient instance;

	public static synchronized ServiceClient getInstance() {
		if (instance == null) {
			instance = new ServiceClient();
		}
		return instance;
	}

	public static String getGetOrders() {
		return GET_ORDERS;
	}

	public static String getGetProducts() {
		return GET_PRODUCTS;
	}

	public static String getGetCustomers() {
		return GET_CUSTOMERS;
	}

	public static String getCreateOrder() {
		return CREATE_ORDER;
	}

	public static String getAddToCart() {
		return ADD_TO_CART;
	}

	public static String getGetCart() {
		return GET_CART;
	}

}
