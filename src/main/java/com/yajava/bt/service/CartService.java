package com.yajava.bt.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yajava.dt.model.Product;

import javax.annotation.PostConstruct;

public class CartService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Product> cartList;
	ServiceClient service = ServiceClient.getInstance();

	public void addToCart(int id) throws IOException {

		URL url = new URL(ServiceClient.getAddToCart() + id);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json; utf-8");
		con.setRequestProperty("Accept", "application/json");
		con.setDoOutput(true);

		try (BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
			StringBuilder response = new StringBuilder();
			String responseLine = null;
			while ((responseLine = br.readLine()) != null) {
				response.append(responseLine.trim());
			}
			System.out.println(response.toString());
		}
	}

	@PostConstruct

	public void init() throws IOException {

		cartList = getCart();

	}
	
	public List<Product> getCart() throws IOException {
		URL url = new URL(ServiceClient.getGetCart());
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.setDoOutput(true);
		BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
		StringBuilder sb = new StringBuilder();
		String output;
		while ((output = br.readLine()) != null) {
			sb.append(output);
		}
		br.close();
		ObjectMapper mapper = new ObjectMapper();

		List<Product> products = mapper.readValue(sb.toString(), new TypeReference<List<Product>>() {
		});

		return products;
	}
}
